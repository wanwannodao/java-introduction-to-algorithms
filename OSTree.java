/*Order Statistic Tree*/
public class OSTree {
	private final int RED = 1;
	private final int BLACK = -1;
	private class Node{
		Node left;
		Node right;
		Node parent;
		int key;
		int size;
		int color;
		private Node(int color,int key){
			this.color = color;
			this.key = key;
		}
		private Node(){}
	}
	private final Node sentinel = new Node();
	
	private Node root;
	
	public OSTree(){root = sentinel;}
	public Node treeSearch(Node x,int value){
		while(x!=sentinel && value != x.key){
			if(value < x.key){
				x = x.left;
			}
			else{
				x = x.right;
			}
		}
		return x;
	}
	public Node minimum(Node x){
		while(x.left != sentinel){x = x.left;}
		return x;
	}
	public Node maximum(Node x){
		while(x.right != sentinel){x = x.right;}
		return x;
	}
	public Node successor(Node x){
		if(x.right != sentinel){
			return minimum(x.right);
		}
		else{
			while(x.parent != sentinel && x == x.parent.right ){
				x = x.parent;
			}
			return x;
		}
	}
	public Node predecessor(Node x){
		if(x.left != sentinel){
			return maximum(x.left);
		}
		else{
			while(x.parent != sentinel && x == x.parent.right){
				 x =  x.parent;
			}
			return x.parent;
		}
	}
	private void leftRotate(Node x){
		Node y = x.right;
		
		x.right = y.left;
		if(y.left != sentinel){
			y.left.parent = x;
		}
		
		y.parent = x.parent;
		if(x.parent == sentinel){
			root = y;
		}else if(x==x.parent.left){
			x.parent.left = y;
		}else{
			x.parent.right = y;
		}
		
		y.left = x;
		x.parent = y;
		
		/*renew sizes*/
		y.size = x.size;
		x.size = x.left.size + x.right.size + 1;
	}
	private void rightRotate(Node x){
		Node y = x.left;
		
		x.left = y.right;
		if(y.right != sentinel){
			y.right.parent = x;
		}
		
		y.parent = x.parent;
		if(x.parent == sentinel){
			root = y;
		}else if(x==x.parent.left){
			x.parent.left = y;
		}else{
			x.parent.right = y;
		}
		y.right = x;
		x.parent = y;
		
		y.size = x.size;
		x.size = x.left.size + x.right.size + 1;
	}
	public void OSInsert(int value){
		Node z = new Node(RED,value);
		Node x = root;
		Node y = sentinel;
		
		while(x!=sentinel){
			y = x;
			
			y.size++;//form root to the new node's parent
			
			if(z.key < x.key){
				x = x.left;
			}
			else{
				x = x.right;
			}
		}
		z.parent = y;
		if(y == sentinel){
			 root = z;
		}
		else if(x == y.left){
			y.left = z;
		}
		else{
			y.right = z;
		}
		z.left = sentinel;
		z.right = sentinel;
		z.size = 1;//new node size
		
		RBInsertFixup(z);
	}
	private void RBInsertFixup(Node z){
		while(z.parent.color == RED){
			if(z.parent == z.parent.parent.left){
				Node w = z.parent.parent.right;
				if(w.color == RED){
					z.parent.color = BLACK;
					w.color = BLACK;
					z.parent.parent.color = RED;
					z = z.parent.parent;
				}else{
					if(z == z.parent.right){
						z = z.parent;
						leftRotate(z);
					}
					z.parent.parent.color = RED;
					z.parent.color = RED;
					rightRotate(z.parent.parent);
				}
			}
			else{
				Node w = z.parent.parent.left;
				if(w.color == RED){
					w.color = BLACK;
					z.parent.color = BLACK;
					z.parent.parent.color = RED;
					z = z.parent.parent;
				}else{
					if(z == z.parent.left){
						z = z.parent;
						rightRotate(z);
					}
					z.parent.parent.color = RED;
					z.parent.color = BLACK;
					leftRotate(z.parent);
				}
			}
		}
		root.color = BLACK;
	}
	private void RBTransplant(Node u,Node v){
		if(u.parent == sentinel){
			root = v;
		}else if(u == u.parent.left){
			u.parent.left = v;
		}else{
			u.parent.right = v;
		}
		v.parent = u.parent;
	}
	public void OSDelete(Node z){
		Node y = z;
		Node x;
		int yOriginalColor = y.color;
		if(z.left == sentinel){
			x = z.right;
			RBTransplant(z,z.right);
		}else if(z.right == sentinel){
			x = z.left;
			RBTransplant(z,z.left);
		}else{
			y = minimum(z.right);
			yOriginalColor = y.color;
			x = y.right;
			if(y==z.right){
				x.parent = y;
			}else{
				RBTransplant(y,y.right);
				y.right = z.right;
				z.right.parent = y;
			}
			RBTransplant(z,y);
			y.left = z.left;
			y.left.parent = y;
			y.color = z.color;
			y.size = z.size;//change size
		}
		Node w = x.parent;
		while(w.parent == sentinel){//from x to root
			w.size--;
			w = w.parent;
		}
		if(yOriginalColor == BLACK){
			RBDeleteFixup(x);
		}
	}
	private void RBDeleteFixup(Node x){
		Node w;
		while(x != root && x.color == BLACK){
			if(x == x.parent.left){
				w = x.parent.right;
				if(w.color == RED){
					x.parent.color = RED;
					w.color = BLACK;
					leftRotate(x.parent);
					w = x.parent.right;
				}
				if(w.left.color == BLACK && w.right.color == BLACK){
					w.color = RED;
					x = x.parent;
				}else{
					if(w.right.color == BLACK){
						w.color = RED;
						w.left.color = BLACK;
						rightRotate(w);
						w = x.parent.right;
					}
					w.color = x.parent.color;
					x.parent.color = BLACK;
					w.right.color = BLACK;
					leftRotate(x.parent);
					x = root;
				}
			}else{
				w = x.parent.left;
				if(w.color == RED){
					x.parent.color = RED;
					w.color = BLACK;
					rightRotate(x.parent);
					w = x.parent.left;
				}
				if(w.left.color == BLACK && w.right.color == BLACK){
					w.color = RED;
					x = x.parent;
				}else{
					if(w.left.color == BLACK){
						w.color = RED;
						w.right.color = BLACK;
						leftRotate(w);
						w = x.parent.left;
					}
					w.color = x.parent.color;
					x.parent.color = BLACK;
					w.left.color = BLACK;
					rightRotate(x.parent);
					x = root;
				}
			}
		}
		x.color = BLACK;
	}
	
	public Node OSSelect(Node x,int i){
		int rank = x.left.size + 1;
		while(rank != i){
			if(i < rank){
				x = x.left;
			}else{
				x = x.right;
				i = i - rank;
			}
			rank = x.left.size + 1;
		}
		return x;
	}
	public int OSRank(Node x){
		int rank = x.left.size + 1;
		Node y = x;
		while(y != root){
			if (y == y.parent.right){
				rank += y.parent.left.size + 1;
			}
			y = y.parent;
		}
		return rank;
	}
	
}
