/*Red-Black Tree*/
public class RBTree {
	private final int RED = 1;
	private final int BLACK = -1;
	private class Node{
		Node left;
		Node right;
		Node parent;
		int color;
		int key;
		private Node(int color,int key){
			this.color = color;
			this.key = key;
		}
		private Node(){}
	}
	private final Node sentinel = new Node();//番兵
	
	private Node root;//根
	
	public RBTree(){ root = sentinel; }
	
	/*探索*/
	public Node treeSearch(Node x,int value){
		while(x != sentinel && value != x.key){
			if(value < x.key){
				x = x.left;
			}else{
				x = x.right;
			}
		}
		return x;
	}
	/*最小値*/
	public Node minimumSearch(Node x){
		while(x.left != sentinel){
			x = x.left;
		}
		return x;
	}
	/*最大値*/
	public Node maximumSearch(Node x){
		while(x.right != sentinel){
			x = x.right;
		}
		return x;
	}
	/*次節点*/
	public Node successor(Node x){
		if(x.right != sentinel){
			return minimumSearch(x.right);
		}
		while(x.parent != sentinel && x == x.parent.right){
			x = x.parent;
		}
		return x.parent;
	}
	/*先行節点*/
	public Node predecessor(Node x){
		if(x.left != sentinel){
			return maximumSearch(x.left);
		}
		while(x.parent != sentinel && x == x.parent.left){
			x = x.parent;
		}
		return x.parent;
	}
	/*左回転*/
	private void leftRotate(Node x){
		Node y = x.right;
		
		x.right = y.left;
		/*yの子とxのリンク*/
		if(y.left != sentinel){
			y.left.parent = x;
		}
		
		/*yとxの親のリンク*/
		y.parent = x.parent;
		if(x.parent == sentinel){
			root = y;
		}else if(x == x.parent.left){
			x.parent.left = y;
		}else{
			x.parent.right = y;
		}
		
		/*yとxのリンク*/
		y.left = x;
		x.parent = y;
		
	}
	/*右回転*/
	private void rightRotate(Node x){
		Node y = x.left;
		
		x.left = y.right;
		if(y.right != sentinel){
			y.right.parent = x;
		}
		
		y.parent = x.parent;
		if(x.parent == sentinel){
			root = y;
		}else if(x == x.parent.left){
			x.parent.left = y;
		}else{
			x.parent.right = y;
		}
		
		y.right = x;
		x.parent = y;
	}
	/*挿入*/
	public void RBInsert(int value){
		Node z = new Node(RED,value);//赤ノードで追加
		Node y = sentinel;//tailing pointer
		Node x = root;
		
		while (x!=sentinel){
			y = x;
			if(z.key < x.key){
				x = x.left;
			}else{
				x = x.right;
			}
		}
		z.parent = y;
		if(y == sentinel){
			root = z; 
		}else if(z.key < y.key){
			y.left = z;
		}else{
			y.right = z;
		}
		/*番兵へのリンク*/
		z.left = sentinel;
		z.right = sentinel;
		
		/*2色条件回復*/
		RBInsertFixup(z);
	}
	/*2色条件修正（挿入）*/
	private void RBInsertFixup(Node z){
		/*ループ内不変式「zは赤」「z.parentが根ならば黒」「違反があるなら2か4」*/
		while(z.parent.color == RED){
			/*親が祖父の左の子*/
			if(z.parent == z.parent.parent.left){
				Node y = z.parent.parent.right;
				/*I)叔父が赤の場合*/
				if(y.color == RED){
					/*色反転*/
					z.parent.color = BLACK;
					y.color = BLACK;
					z.parent.parent.color = RED;
					z = z.parent.parent;//2レベル上昇
				}
				/*Ⅱ)叔父が黒,zは右の子*/
				else {
					if(z == z.parent.right){
						z = z.parent;
						leftRotate(z);
					}
					/*Ⅲ)叔父が黒,zは左の子*/
					z.parent.color = BLACK;
					z.parent.parent.color = RED;
					rightRotate(z.parent.parent);
				}
			}
			/*親が祖父の右の子*/
			else{
				Node y = z.parent.parent.left;
				if(y.color == RED){
					y.color = BLACK;
					z.parent.color = BLACK;
					z.parent.parent.color = RED;
					z = z.parent.parent;
				}else{
					if(z == z.parent.left){
						z = z.parent;
						rightRotate(z);
					}
					z.parent.color = BLACK;
					z.parent.parent.color = RED;
					leftRotate(z.parent.parent);
				}
			}
		}
		root.color = BLACK;
	}
	/*部分木差し替え*/
	private void RBTransplant(Node u,Node v){
		if(u.parent == sentinel){
			root = v;
		}else if(u == u.parent.left){
			u.parent.left = v;
		}else{
			u.parent.right = v;
		}
		v.parent = u.parent;
	}
	/*削除*/
	public void RBDelete(Node z){
		Node y = z;
		Node x;
		int yOriginalColor = y.color;
		if(z.left == sentinel){
			x = z.right;
			RBTransplant(z,z.right);
		}else if(z.right == sentinel){
			x = z.left;
			RBTransplant(z,z.left);
		}else{
			y = successor(z);//zの次節点
			yOriginalColor = y.color;
			x = y.right;
			if(y.parent == z){
				x.parent = y;
			}else{
				RBTransplant(y,y.right);
				y.right = z.right;
				y.right.parent = y;
			}
			RBTransplant(z,y);
			y.left = z.left;
			y.left.parent = y;
			y.color = z.color;
		}
		if(yOriginalColor == BLACK){
			RBDeleteFixup(x);
		}
	}
	/*2色条件修正（削除）*/
	private void RBDeleteFixup(Node x){
		while(x != root && x.color == BLACK){
			if(x == x.parent.left){
				Node w = x.parent.right;
				/*Ⅰ)「兄弟が赤の場合」色交換＋左回転*/
				if(w.color == RED){
					w.color = BLACK;
					x.parent.color = RED;
					leftRotate(x.parent);
					w = x.parent.right;
				}
				/*Ⅱ)「兄弟が黒＋その両方の子が黒」特黒をプッシュ*/
				if(w.left.color == BLACK && w.right.color == BLACK){
					w.color = RED;
					x = x.parent;
				}
				else{
					/*Ⅲ)「兄弟が黒+右の子が黒、左の子が赤」色反転＋右回転*/
					if(w.right.color == BLACK){
						w.left.color = BLACK;
						w.color = RED;
						rightRotate(w);
						w = x.parent.right;
					}
					/*Ⅳ)「兄弟が黒＋右の子が赤」再彩色＋左回転*/
					w.color = x.parent.color;
					x.parent.color = BLACK;
					w.right.color = BLACK;
					leftRotate(x.parent);
					x = root;
				}
			}else{
				Node w = x.parent.left;
				if(w.color == RED){
					w.color = BLACK;
					x.parent.color = RED;
					rightRotate(x.parent);
					w = x.parent.left;
				}
				if(w.left.color == BLACK && w.right.color == BLACK){
					w.color = RED;
					x = x.parent;
				}else{
					if(w.left.color == BLACK){
						w.right.color = BLACK;
						w.color = RED;
						leftRotate(w);
						w = x.parent.left;
					}
					w.color = x.parent.color;
					w.left.color = BLACK;
					x.parent.color = BLACK;
					rightRotate(x.parent);
					x = root;
				}
			}
		}
		x.color = BLACK;
	}
}
