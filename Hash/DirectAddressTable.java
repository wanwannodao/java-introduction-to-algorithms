/*Direct Address Table*/
public class DirectAddressTable {
	Element[] DATable;
	
	private class Element{
		int data;//any class
		int key;
	}
	
	public DirectAddressTable(int size){
		DATable = new Element[size];
	}
	
	public Element DASearch(int key){
		return DATable[key];
	}
	public void DAInsert(Element x){
		DATable[x.key] = x;
	}
	public void DADelet(Element x){
		DATable[x.key] = null;
	}
}
