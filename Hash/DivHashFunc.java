/*Division Method*/
public class DivHashFunc extends HashFunction{
	public DivHashFunc(int m){
		this.m = m;
	}
	
	@Override
	public int hash(int k){
		return k % m;
	}
}
