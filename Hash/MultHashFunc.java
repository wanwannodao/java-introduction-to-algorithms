/*Multiplication Method*/
/*A(0<A<1) is a constant*/
public class MultHashFunc extends HashFunction {
	double A;
	public MultHashFunc(int m,double A){
		this.m = m;
		this.A = A;
	}
	
	@Override
	public int hash(int k){
		return (int)Math.floor(m*((k*A)%1));// floor(m(kA mod 1))
		
		/*
		int p = 0;//m = 2^p
		int t = 1;
		while(t != m){
			t = t << 1;
			p++;
		}
		int s = A << 32;// s = k*A*2^32
		return (s*k - ((s*k >> 32) << 32)) >> (32-p);*/
	}
}
