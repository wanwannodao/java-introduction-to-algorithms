/*super class*/
/*m is the size of a hash table*/
abstract class HashFunction {
	int m;
	abstract int hash(int k);
}
