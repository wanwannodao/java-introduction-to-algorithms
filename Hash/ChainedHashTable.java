/*Chained Hash Table*/
public class ChainedHashTable {
	int m;
	DoublyLinkedList[] T;
	HashFunction h;
	
	public ChainedHashTable(int size,HashFunction func){
		this.m = size;
		this.T = new DoublyLinkedList[size];
		this.h = func;
	}
	
	public ListNode chainedHashSearch(int key){
		return T[h.hash(key)].listSearch(key);
	}
	
	public void chainedHashInsert(ListNode x){
		T[h.hash(x.key)].listInsert(x);
	}
	
	public void chainedHashDelete(ListNode x){
		T[h.hash(x.key)].listDelete(x);
	}
}
