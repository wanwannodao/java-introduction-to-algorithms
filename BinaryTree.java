/*Binary Tree*/
public class BinaryTree {
	
	private class Node{
		Node parent;
		Node left;
		Node right;
		int key;
		private Node(int value){
			key = value;
		}
	}
	
	private Node root = null;
	
	/*挿入操作*/
	public void insert(int value){
		Node y = null;
		Node x = root;
		Node z = new Node(value);
		while(x!=null){
			y = x;
			if(z.key < x.key){
				x = x.left;
			}
			else{
				x = x.right;
			}
		}
		z.parent = y;
		if(y==null){
			root = z;
		}
		else if(z.key < y.key){
			y.left = z;
		}
		else{
			y.right = z;
		}
	}
	
	/*探索*/
	public Node treeSearch(Node x,int value){
		while(x != null && value != x.key){
			if(value < x.key){
				x = x.left;
			}
			else{
				x = x.right;
			}
		}
		return x;
	}
	/*最小値探索*/
	public Node minimumSearch(Node x){
		while(x.left != null){
			x = x.left;
		}
		return x;
	}
	/*最大値探索*/
	public Node maximumSearch(Node x){
		while(x.right != null){
			x = x.right;
		}
		return x;
	}
	/*次節点探索*/
	public Node successor(Node x){
		if(x.right != null){
			return minimumSearch(x.right);//右部分木がある場合は右部分木の最左節点（最小値）
		}
		
		/*Node y = x.parent;
		while(y != null && x == y.right){//xがyの左の子になったら終了
			x = y;
			y = y.parent;
		}
		return y;*/
		while(x.parent != null && x==x.parent.right){
			x = x.parent;
		}
		return x.parent;
	}
	
	/*部分木差し替え、　削除操作サブルーチン*/
	private void transplant(Node u,Node v){
		if(u.parent == null){
			root = v;
		}
		else if(u == u.parent.left){
			u.parent.left = v;
		}
		else{
			u.parent.right = v;
		}
		if(v != null){
			v.parent = u.parent;
		}
	}
	
	/*削除*/
	public void delete(Node x){
		if(x.left == null){//左部分木なし
			transplant(x,x.right);
		}
		else if(x.right == null){//右部分木なし
			transplant(x,x.left);
		}
		else{//両側に子あり
			Node y = successor(x);
			if(y.parent != x){//xの次節点がxの右の子ではない
				transplant(y,y.right);
				y.right = x.right;
				y.right.parent = y;
			}
			transplant(x,y);
			y.left = x.left;
			y.left.parent = y;
		}
	}

}
